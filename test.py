import sys
from subprocess import Popen, PIPE, STDOUT
import json
import requests

'''
For testing tools:
    https://gitlab.com/tilluteenused/docker-elg-tokenizer
    https://gitlab.com/tilluteenused/docker-elg-morf
    https://gitlab.com/tilluteenused/docker-elg-disamb
'''

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-i', '--indent', action="store_true", required=False,
                           help='väljundisse taanetega json')
    args = argparser.parse_args()

    print("\ntokens ===========")

    tok_query_json = json.loads('{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad"}')
    tok_resp = requests.post('http://localhost:6000/process', json=tok_query_json)
    tok_resp_text = tok_resp.content.decode('utf-8')
    tok_resp_json = json.loads(tok_resp_text)

    if args.indent is True:
        json.dump(tok_query_json, sys.stdout, indent=4)
        print('\n----------')
        json.dump(tok_resp_json, sys.stdout, indent=4)
    else:
        json.dump(tok_query_json, sys.stdout)
        print('\n----------')
        json.dump(tok_resp_json, sys.stdout)

    print("\nmorph ===========")

    mrf_query_json = {"type": "text", "content": tok_query_json["content"],
                      "annotations": tok_resp_json['response']["annotations"]}
    mrf_query_txt = json.dumps(mrf_query_json)
    mrf_query_json = json.loads(mrf_query_txt)
    mrf_resp = requests.post('http://localhost:7000/process', json=mrf_query_json)
    mrf_resp_text = mrf_resp.content.decode('utf-8')
    mrf_resp_json = json.loads(mrf_resp_text)

    if args.indent is True:
        json.dump(mrf_resp_json, sys.stdout, indent=4)
        print('\n----------')
        json.dump(mrf_resp_json, sys.stdout, indent=4)
    else:
        json.dump(mrf_resp_json, sys.stdout)
        print('\n----------')
        json.dump(mrf_resp_json, sys.stdout)

    print("\ndisamb ===========")

    dis_query_json = {"type": "text", "content": tok_query_json["content"],
                      "annotations": mrf_resp_json["response"]["annotations"]}
    dis_query_txt = json.dumps(dis_query_json)
    p = Popen(['curl', '-i', '--request', 'POST', '--header', 'Content-Type: application/json', '--data', dis_query_txt,
                                            'localhost:8000/process'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    dis_text_out = p.communicate(input=bytes(dis_query_txt, 'utf-8'))[0].decode('utf-8').split('\r\n')
    dis_answer_txt = dis_text_out[len(dis_text_out)-1]
    dis_answer_json = json.loads(dis_answer_txt)
    if args.indent is True:
        json.dump(dis_query_json, sys.stdout, indent=4)
        print('\n----------')
        json.dump(dis_answer_json, sys.stdout, indent=4)
    else:
        json.dump(dis_query_json, sys.stdout)
        print('\n----------')
        json.dump(dis_answer_json, sys.stdout)

