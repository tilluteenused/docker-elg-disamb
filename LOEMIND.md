## Eesti keele morfoloogilise ühestaja konteiner

[Filosofti morfoloogilist ühestajat](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmety/LOEMIND.md)  sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [Filosofti eesti keele morfoloogiline ühestaja](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmety/LOEMIND.md)
* [Filosofti eesti keele morfoloogiline analüsaator](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/LOEMIND.md)
* [EstNLTK](https://github.com/estnltk/estnltk) lausestaja-sõnestaja 
* Konteineri ja liidesega seotud lähtekood


## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara; juhised on [git'i veebilehel](https://git-scm.com/).

## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/vabamorf_disamb:1.0.0
```
Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. Lähtekoodi allalaadimine

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker-elg-disamb.git gitlab-docker-elg-disamb
```

Repositoorium sisaldab kompileeritud [Filosoft morfoloogilist analüsaatorit](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/LOEMIND.md) ja [ühestajat](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmety/LOEMIND.md):

* **_vmeta_** - morfoloogilise analüüsi programm
* **_vmety_** - morfoloogilise ühestamise programm
* **_et.dct_** - morfoloogilise analüüsi programmi poolt kasutatav leksikon
* **_et3.dct_** - morfoloogilise ühestamise programmi poolt kasutatav leksikon

Kui soovite ise programme (**_vmeta_**, **_vmety_**) kompileerida või leksikone (**_et.dct_**, **_et3.dct_**) täiendada/muuta ja uuesti kokku panna, 
vaadake sellekohast [juhendit](https://github.com/Filosoft/vabamorf/blob/master/doc/programmid_ja_sonastikud.md).

### 2. Konteineri kokkupanemine

```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-disamb
docker build -t tilluteenused/vabamorf_disamb:1.0.0 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 8000:8000 tilluteenused/vabamorf_disamb:1.0.0
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

Tasub tähele panna, et Python'i json'i teek esitab teksti vaikimisi ASCII kooditabelis;
täpitähed jms esitatakse Unicode'i koodidena, nt. õ = \u00f5.

### Variant 1. 

Sisendiks on lihttekst (mille lausestamine ja sõnestamine toimub jooksvalt [EstNLTK](https://github.com/estnltk/estnltk) lausestaja-sõnestaja abil; 
morfoloogiline analüüs [Filosoft morfoloogilise analüsaatori](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/LOEMIND.md) abil).

```json
{
  "type":"text",
  "content": string, /* "Analüüsimist vajav tekst" */
  "params":{...}     /* võib puududa */
}
```

### Variant 2.

Sisendiks on lausestatud, sõnestatud ja morfoloogiliselt analüüsitud tekst. 
Selle tegemiseks saab kasutada [lausestamise-sõnestamise-konteinerit](https://gitlab.com/tarmo.vaino/docker-elg-tokenizer)
ja seejärel [morfoloogilise analüüsi konteinerit](https://gitlab.com/tarmo.vaino/docker-elg-morf).


```json
{
  "type": "text",
  "content": string,     /* "Analüüsimist vajav tekst" */
  "annotations": {       /* võib puududa */
    "sentence": [        /* lausete massiiv */
      {
        "start": number, /* lause algus tekstis (märgijadas) */
        "end": number    /* lause lõpp tekstis (märgijadas) */
      }
    ],
    "token": [           /* kõigi lausete kõigi sõnede massiiv */
      {
        "start": number, /* sõne algus tekstis (märgijadas) */
        "end": number,   /* sõne lõpp tekstis (märgijadas) */
        "features": {
          "token": string /* sõne */
          "morph": [
            {
              "lemma":string,    /* algvorm e. lemma */
              "pos":string,      /* sõnaliik */
              "features":string  /* morf. info */
            }
          ]            
        }
      }
    ]
  }
}
```

## Vastuse json-kuju

```json
{
  "response":
  {
    "type":"annotations",
    "annotations": {
      "sentence": [        /* lausete massiiv */
        {
          "start": number,     /* lause algus tekstis (märgijadas) */
          "end": number        /* lause lõpp tekstis (märgijadas) */
        }
      ],
      "token": [           /* kõigi lausete kõigi sõnede massiiv */
        {
          "start": number, /* sõne algus tekstis (märgijadas) */
          "end": number,   /* sõne lõpp tekstis (märgijadas) */
          "features": {
            "token": string /* sõne */
            "morph": [
              {
                "lemma":string,    /* algvorm e. lemma */
                "pos":string,      /* sõnaliik */
                "features":string  /* morf. info */
              }
            ]            
          }
        }
      ]
    }
  }
}
```

Väljundis (nagu ka sisendis) kasutatakse [Filosofti tähistust](https://github.com/Filosoft/vabamorf/blob/master/doc/kategooriad.md).
Ühestaja kustutab konteksti põhjal enamuse liigsetest analüüsidest, aga väljund ei ole 100% ühene.
*NB! Variandid POLE usutavuse järjekorras!*

## Kasutusnäited

### Näide 1. 

Sisendiks on lihttekst.

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}' http://localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Sun, 20 Feb 2022 19:46:25 GMT
Connection: close
Content-Type: application/json
Content-Length: 846

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees","morph":[{"lemma":"mees+0","pos":"S","feature":"sg n"}]}},{"start":5,"end":10,"features":{"token":"peeti","morph":[{"lemma":"pida+ti","pos":"V","feature":"ti"}]}},{"start":11,"end":16,"features":{"token":"kinni","morph":[{"lemma":"kinni+0","pos":"D","feature":""}]}},{"start":16,"end":17,"features":{"token":".","morph":[{"lemma":".","pos":"Z","feature":""}]}},{"start":18,"end":24,"features":{"token":"Sarved","morph":[{"lemma":"sarv+d","pos":"S","feature":"pl n"}]}},{"start":24,"end":25,"features":{"token":"&","morph":[{"lemma":"&+0","pos":"J","feature":""}]}},{"start":25,"end":30,"features":{"token":"S\u00f5rad","morph":[{"lemma":"s\u00f5rg+d","pos":"S","feature":"pl n"}]}}]}}}
```

### Näide 2. 

Sisendiks on lausestatud, sõnestatud ja morfoloogiliselt analüüsitud tekst.

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad", "annotations": {"sentence": [{"start": 0, "end": 17}, {"start": 18, "end": 30}], "token": [{"start": 0, "end": 4, "features": {"token": "Mees", "morph": [{"lemma": "Mee+s", "pos": "H", "feature": "sg in"}, {"lemma": "Mees+0", "pos": "H", "feature": "sg n"}, {"lemma": "Mesi+s", "pos": "H", "feature": "sg in"}, {"lemma": "mees+0", "pos": "S", "feature": "sg n"}, {"lemma": "mesi+s", "pos": "S", "feature": "sg in"}]}}, {"start": 5, "end": 10, "features": {"token": "peeti", "morph": [{"lemma": "peet+0", "pos": "S", "feature": "adt"}, {"lemma": "pida+ti", "pos": "V", "feature": "ti"}, {"lemma": "peet+0", "pos": "S", "feature": "sg p"}]}}, {"start": 11, "end": 16, "features": {"token": "kinni", "morph": [{"lemma": "kinni+0", "pos": "D", "feature": ""}]}}, {"start": 16, "end": 17, "features": {"token": ".", "morph": [{"lemma": ".", "pos": "Z", "feature": ""}]}}, {"start": 18, "end": 24, "features": {"token": "Sarved", "morph": [{"lemma": "Sarv+d", "pos": "H", "feature": "pl n"}, {"lemma": "Sarve+d", "pos": "H", "feature": "pl n"}, {"lemma": "Sarved+0", "pos": "H", "feature": "sg n"}, {"lemma": "sarv+d", "pos": "S", "feature": "pl n"}]}}, {"start": 24, "end": 25, "features": {"token": "&", "morph": [{"lemma": "&+0", "pos": "J", "feature": ""}]}}, {"start": 25, "end": 30, "features": {"token": "S\u00f5rad", "morph": [{"lemma": "S\u00f5ra+d", "pos": "H", "feature": "pl n"}, {"lemma": "S\u00f5rad+0", "pos": "H", "feature": "sg n"}, {"lemma": "S\u00f5rg+d", "pos": "H", "feature": "pl n"}, {"lemma": "s\u00f5rg+d", "pos": "S", "feature": "pl n"}]}}]}}' http://localhost:8000/process
```

```json
HTTP/1.1 100 Continue

HTTP/1.1 200 OK
Server: gunicorn
Date: Sat, 05 Feb 2022 20:02:00 GMT
Connection: close
Content-Type: application/json
Content-Length: 846

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees","morph":[{"lemma":"mees+0","pos":"S","feature":"sg n"}]}},{"start":5,"end":10,"features":{"token":"peeti","morph":[{"lemma":"pida+ti","pos":"V","feature":"ti"}]}},{"start":11,"end":16,"features":{"token":"kinni","morph":[{"lemma":"kinni+0","pos":"D","feature":""}]}},{"start":16,"end":17,"features":{"token":".","morph":[{"lemma":".","pos":"Z","feature":""}]}},{"start":18,"end":24,"features":{"token":"Sarved","morph":[{"lemma":"sarv+d","pos":"S","feature":"pl n"}]}},{"start":24,"end":25,"features":{"token":"&","morph":[{"lemma":"&+0","pos":"J","feature":""}]}},{"start":25,"end":30,"features":{"token":"S\u00f5rad","morph":[{"lemma":"s\u00f5rg+d","pos":"S","feature":"pl n"}]}}]}}}
```


## Vaata lisaks

* [Eesti keele lausestaja-sõnestaja konteiner](https://gitlab.com/tilluteenused/docker-elg-tokenizer/)
* [Eesti keele morfoloogilise analüsaatori konteiner](https://gitlab.com/tilluteenused/docker-elg-morf/) ja [käsureaprogramm](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/LOEMIND.md)
* [Eesti keele morfoloogilise ühestaja käsureaprogramm](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmety/LOEMIND.md)

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
 
