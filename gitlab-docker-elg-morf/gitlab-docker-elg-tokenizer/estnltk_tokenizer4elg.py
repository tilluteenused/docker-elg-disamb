#!/usr/bin/env python3

from estnltk import Text
from estnltk.taggers import SentenceTokenizer
from typing import Dict


def estnltk_lausesta_text4elg(text_in: str) -> Dict:
    '''
    Find sentences and tokens
    :param text_in: input text
    :return: sentence and token boundaries in ELG-annotation format
    '''
    estnltk_text = Text(text_in)
    estnltk_text.tag_layer(['words'])
    SentenceTokenizer().tag(estnltk_text)
    sentences = []
    tokens = []
    for sentence in estnltk_text.sentences:
        for word in sentence:
            tokens.append({"start": word.start, "end": word.end, "features": {"token": word.enclosing_text}})
        sentences.append({"start": sentence.start, "end": sentence.end})
        assert (tokens[0]["start"] == sentences[0]["start"])
        assert (tokens[-1]["end"] == sentences[-1]["end"])
    return {"sentence": sentences, "token": tokens}
