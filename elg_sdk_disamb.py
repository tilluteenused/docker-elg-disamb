#!/usr/bin/env python3
'''
Lähtekoodist pythoni skripti käivitamine
1 Lähtekoodi allalaadimine (1.1), virtuaalkeskkonna loomine (1.2), pythoni skripti kasutamine (1.3)
    $ mkdir -p ~/git/ ; cd ~/git/
    $ git clone https://gitlab.com/tilluteenused/docker-elg-disamb.git docker_elg_disamb_gitlab
1.2 Virtuaalkeskkonna loomine
    $ cd ~/git/docker_elg_disamb_gitlab
    $ ./create_venv.sh
1.3 Pythoni skripti kasutamine
    $ venv/bin/python3 ./elg_sdk_disamb.py --json='{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}'
    $ venv/bin/python3 ./elg_sdk_disamb.py --json='{"params":{"vmety":["--gt"]}, "type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}'
    $ venv/bin/python3 ./elg_sdk_disamb.py --json='{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad", "annotations": {"sentences": [{"start": 0, "end": 17, "features": {"tokens": [{"start": 0, "end": 4, "token": "Mees", "morph": [{"lemma": "Mee+s", "pos": "H", "features": "sg in"}, {"lemma": "Mees+0", "pos": "H", "features": "sg n"}, {"lemma": "Mesi+s", "pos": "H", "features": "sg in"}, {"lemma": "mees+0", "pos": "S", "features": "sg n"}, {"lemma": "mesi+s", "pos": "S", "features": "sg in"}]}, {"start": 5, "end": 10, "token": "peeti", "morph": [{"lemma": "peet+0", "pos": "S", "features": "adt"}, {"lemma": "pida+ti", "pos": "V", "features": "ti"}, {"lemma": "peet+0", "pos": "S", "features": "sg p"}]}, {"start": 11, "end": 16, "token": "kinni", "morph": [{"lemma": "kinni+0", "pos": "D", "features": ""}]}, {"start": 16, "end": 17, "token": ".", "morph": [{"lemma": ".", "pos": "Z", "features": ""}]}]}}, {"start": 18, "end": 30, "features": {"tokens": [{"start": 18, "end": 24, "token": "Sarved", "morph": [{"lemma": "Sarv+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarve+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarved+0", "pos": "H", "features": "sg n"}, {"lemma": "sarv+d", "pos": "S", "features": "pl n"}]}, {"start": 24, "end": 25, "token": "&", "morph": [{"lemma": "&+0", "pos": "J", "features": ""}]}, {"start": 25, "end": 30, "token": "S\u00f5rad", "morph": [{"lemma": "S\u00f5ra+d", "pos": "H", "features": "pl n"}, {"lemma": "S\u00f5rad+0", "pos": "H", "features": "sg n"}, {"lemma": "S\u00f5rg+d", "pos": "H", "features": "pl n"}, {"lemma": "s\u00f5rg+d", "pos": "S", "features": "pl n"}]}]}}]}}'

----------------------------------------------

Lähtekoodist pythoni serveri käivitamine & kasutamine
2 Lähtekoodi allalaadimine (2.1), virtuaalkeskkonna loomine (2.2), veebiteenuse käivitamine pythoni koodist (2.3) ja CURLiga veebiteenuse kasutamise näited (2.4)
2.1 Lähtekoodi allalaadimine: järgi punkti 1.1
2.2 Virtuaalkeskkonna loomine: järgi punkti 1.2
    $ cd ~/git/docker_elg_disamb_gitlab
    $ ./create_venv.sh
2.3 Veebiteenuse käivitamine pythoni koodist
    $ venv/bin/python3 ./elg_sdk_disamb.py
2.4 CURLiga veebiteenuse kasutamise näited
   $ curl --silent --request POST --header "Content-Type: application/json" \
        --data '{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}' \
        http://localhost:8000/process
    $ curl --silent --request POST --header "Content-Type: application/json" \
        --data '{"params":{"vmety":["--gt"]}, "type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}'  \
        http://localhost:8000/process
    $ curl --silent --request POST --header "Content-Type: application/json" \
        --data '{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad", "annotations": {"sentences": [{"start": 0, "end": 17, "features": {"tokens": [{"start": 0, "end": 4, "token": "Mees", "morph": [{"lemma": "Mee+s", "pos": "H", "features": "sg in"}, {"lemma": "Mees+0", "pos": "H", "features": "sg n"}, {"lemma": "Mesi+s", "pos": "H", "features": "sg in"}, {"lemma": "mees+0", "pos": "S", "features": "sg n"}, {"lemma": "mesi+s", "pos": "S", "features": "sg in"}]}, {"start": 5, "end": 10, "token": "peeti", "morph": [{"lemma": "peet+0", "pos": "S", "features": "adt"}, {"lemma": "pida+ti", "pos": "V", "features": "ti"}, {"lemma": "peet+0", "pos": "S", "features": "sg p"}]}, {"start": 11, "end": 16, "token": "kinni", "morph": [{"lemma": "kinni+0", "pos": "D", "features": ""}]}, {"start": 16, "end": 17, "token": ".", "morph": [{"lemma": ".", "pos": "Z", "features": ""}]}]}}, {"start": 18, "end": 30, "features": {"tokens": [{"start": 18, "end": 24, "token": "Sarved", "morph": [{"lemma": "Sarv+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarve+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarved+0", "pos": "H", "features": "sg n"}, {"lemma": "sarv+d", "pos": "S", "features": "pl n"}]}, {"start": 24, "end": 25, "token": "&", "morph": [{"lemma": "&+0", "pos": "J", "features": ""}]}, {"start": 25, "end": 30, "token": "S\u00f5rad", "morph": [{"lemma": "S\u00f5ra+d", "pos": "H", "features": "pl n"}, {"lemma": "S\u00f5rad+0", "pos": "H", "features": "sg n"}, {"lemma": "S\u00f5rg+d", "pos": "H", "features": "pl n"}, {"lemma": "s\u00f5rg+d", "pos": "S", "features": "pl n"}]}]}}]}}' \
        http://localhost:8000/process
    $ curl --silent --request POST --header "Content-Type: application/json" \
        --data '{"params":{"vmety":["--gt"]}, "type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad", "annotations": {"sentences": [{"start": 0, "end": 17, "features": {"tokens": [{"start": 0, "end": 4, "token": "Mees", "morph": [{"lemma": "Mee+s", "pos": "H", "features": "sg in"}, {"lemma": "Mees+0", "pos": "H", "features": "sg n"}, {"lemma": "Mesi+s", "pos": "H", "features": "sg in"}, {"lemma": "mees+0", "pos": "S", "features": "sg n"}, {"lemma": "mesi+s", "pos": "S", "features": "sg in"}]}, {"start": 5, "end": 10, "token": "peeti", "morph": [{"lemma": "peet+0", "pos": "S", "features": "adt"}, {"lemma": "pida+ti", "pos": "V", "features": "ti"}, {"lemma": "peet+0", "pos": "S", "features": "sg p"}]}, {"start": 11, "end": 16, "token": "kinni", "morph": [{"lemma": "kinni+0", "pos": "D", "features": ""}]}, {"start": 16, "end": 17, "token": ".", "morph": [{"lemma": ".", "pos": "Z", "features": ""}]}]}}, {"start": 18, "end": 30, "features": {"tokens": [{"start": 18, "end": 24, "token": "Sarved", "morph": [{"lemma": "Sarv+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarve+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarved+0", "pos": "H", "features": "sg n"}, {"lemma": "sarv+d", "pos": "S", "features": "pl n"}]}, {"start": 24, "end": 25, "token": "&", "morph": [{"lemma": "&+0", "pos": "J", "features": ""}]}, {"start": 25, "end": 30, "token": "S\u00f5rad", "morph": [{"lemma": "S\u00f5ra+d", "pos": "H", "features": "pl n"}, {"lemma": "S\u00f5rad+0", "pos": "H", "features": "sg n"}, {"lemma": "S\u00f5rg+d", "pos": "H", "features": "pl n"}, {"lemma": "s\u00f5rg+d", "pos": "S", "features": "pl n"}]}]}}]}}' \
        http://localhost:8000/process

----------------------------------------------

Lähtekoodist tehtud konteineri kasutamine
3 Lähtekoodi allalaadimine (3.1), konteineri kokkupanemine (3.2), konteineri käivitamine (3.3) ja CURLiga veebiteenuse kasutamise näited (3.4)
3.1 Lähtekoodi allalaadimine: järgi punkti 1.1
3.2 Konteineri kokkupanemine
    $ cd ~/git/docker_elg_disamb_gitlab
    $ docker build -t tilluteenused/vabamorf_disamb:2023.09.12 . 
3.3 Konteineri käivitamine
    $ docker run -p 8000:800  tilluteenused/vabamorf_disamb:2023.09.12
3.4 CURLiga veebiteenuse kasutamise näited: järgi punkti 2.4

----------------------------------------------

DockerHUBist tõmmatud konteineri kasutamine
4 DockerHUBist koneineri allalaadimine (4.1), konteineri käivitamine (4.2) ja CURLiga veebiteenuse kasutamise näited (4.3)
4.1 DockerHUBist konteineri allalaadimine
    $ docker pull tilluteenused/vabamorf_disamb:2023.09.12
4.2 Konteineri käivitamine: järgi punkti 3.3
4.3 CURLiga veebiteenuse kasutamise näited: järgi punkti 2.4

----------------------------------------------

TÜ pilves töötava konteineri kasutamine
4 CURLiga veebiteenuse kasutamise näited
    $ curl --silent --request POST --header "Content-Type: application/json" \
        --data '{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}' \
        https://smart-search.tartunlp.ai/api/disamb/process | jq
    $ curl --silent --request POST --header "Content-Type: application/json" \
        --data '{"params":{"vmety":["--gt"]}, "type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad"}'  \
        https://smart-search.tartunlp.ai/api/disamb/process | jq
    $ curl --silent --request POST --header "Content-Type: application/json" \
        --data '{"type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad", "annotations": {"sentences": [{"start": 0, "end": 17, "features": {"tokens": [{"start": 0, "end": 4, "token": "Mees", "morph": [{"lemma": "Mee+s", "pos": "H", "features": "sg in"}, {"lemma": "Mees+0", "pos": "H", "features": "sg n"}, {"lemma": "Mesi+s", "pos": "H", "features": "sg in"}, {"lemma": "mees+0", "pos": "S", "features": "sg n"}, {"lemma": "mesi+s", "pos": "S", "features": "sg in"}]}, {"start": 5, "end": 10, "token": "peeti", "morph": [{"lemma": "peet+0", "pos": "S", "features": "adt"}, {"lemma": "pida+ti", "pos": "V", "features": "ti"}, {"lemma": "peet+0", "pos": "S", "features": "sg p"}]}, {"start": 11, "end": 16, "token": "kinni", "morph": [{"lemma": "kinni+0", "pos": "D", "features": ""}]}, {"start": 16, "end": 17, "token": ".", "morph": [{"lemma": ".", "pos": "Z", "features": ""}]}]}}, {"start": 18, "end": 30, "features": {"tokens": [{"start": 18, "end": 24, "token": "Sarved", "morph": [{"lemma": "Sarv+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarve+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarved+0", "pos": "H", "features": "sg n"}, {"lemma": "sarv+d", "pos": "S", "features": "pl n"}]}, {"start": 24, "end": 25, "token": "&", "morph": [{"lemma": "&+0", "pos": "J", "features": ""}]}, {"start": 25, "end": 30, "token": "S\u00f5rad", "morph": [{"lemma": "S\u00f5ra+d", "pos": "H", "features": "pl n"}, {"lemma": "S\u00f5rad+0", "pos": "H", "features": "sg n"}, {"lemma": "S\u00f5rg+d", "pos": "H", "features": "pl n"}, {"lemma": "s\u00f5rg+d", "pos": "S", "features": "pl n"}]}]}}]}}' \
        https://smart-search.tartunlp.ai/api/disamb/process | jq
    $ curl --silent --request POST --header "Content-Type: application/json" \
        --data '{"params":{"vmety":["--gt"]}, "type": "text", "content": "Mees peeti kinni. Sarved&S\u00f5rad", "annotations": {"sentences": [{"start": 0, "end": 17, "features": {"tokens": [{"start": 0, "end": 4, "token": "Mees", "morph": [{"lemma": "Mee+s", "pos": "H", "features": "sg in"}, {"lemma": "Mees+0", "pos": "H", "features": "sg n"}, {"lemma": "Mesi+s", "pos": "H", "features": "sg in"}, {"lemma": "mees+0", "pos": "S", "features": "sg n"}, {"lemma": "mesi+s", "pos": "S", "features": "sg in"}]}, {"start": 5, "end": 10, "token": "peeti", "morph": [{"lemma": "peet+0", "pos": "S", "features": "adt"}, {"lemma": "pida+ti", "pos": "V", "features": "ti"}, {"lemma": "peet+0", "pos": "S", "features": "sg p"}]}, {"start": 11, "end": 16, "token": "kinni", "morph": [{"lemma": "kinni+0", "pos": "D", "features": ""}]}, {"start": 16, "end": 17, "token": ".", "morph": [{"lemma": ".", "pos": "Z", "features": ""}]}]}}, {"start": 18, "end": 30, "features": {"tokens": [{"start": 18, "end": 24, "token": "Sarved", "morph": [{"lemma": "Sarv+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarve+d", "pos": "H", "features": "pl n"}, {"lemma": "Sarved+0", "pos": "H", "features": "sg n"}, {"lemma": "sarv+d", "pos": "S", "features": "pl n"}]}, {"start": 24, "end": 25, "token": "&", "morph": [{"lemma": "&+0", "pos": "J", "features": ""}]}, {"start": 25, "end": 30, "token": "S\u00f5rad", "morph": [{"lemma": "S\u00f5ra+d", "pos": "H", "features": "pl n"}, {"lemma": "S\u00f5rad+0", "pos": "H", "features": "sg n"}, {"lemma": "S\u00f5rg+d", "pos": "H", "features": "pl n"}, {"lemma": "s\u00f5rg+d", "pos": "S", "features": "pl n"}]}]}}]}}' \
        https://smart-search.tartunlp.ai/api/disamb/process | jq
'''


import sys
import os
import json
sys.path.append(os.path.abspath('gitlab-docker-elg-morf'))
sys.path.append(os.path.abspath('gitlab-docker-elg-morf/gitlab-docker-elg-tokenizer'))
import filosoft_morph
import estnltk_tokenizer4elg

from subprocess import Popen, PIPE, STDOUT
from typing import Dict, List

from elg import FlaskService
from elg.model import AnnotationsResponse

# from inspect import currentframe, getframeinfo
# print(getframeinfo(currentframe()).filename, getframeinfo(currentframe()).lineno)


class FiloSoft_disamb(FlaskService):
    def process_text(self, request):
        sentences = []
        tokens_in = []
        tokens_out = []
        cmd = ['./vmety', '--path', '.']
        if request.params is not None and "vmety" in request.params:
            cmd += request.params["vmety"]
        if (request.annotations is None) or not ("sentence" in request.annotations.keys() and "token" in request.annotations.keys()):
            #  Should have content. Plain text, needs tokenization and morph analyser
            request = AnnotationsResponse(annotations=estnltk_tokenizer4elg.estnltk_lausesta_text4elg(request.content))
            sentences = request.annotations["sentence"]
            tokens_in = request.annotations["token"]
            tokens_in = filosoft_morph.filosoft_morph4elg(sentences, tokens_in, path='gitlab-docker-elg-morf')
        else:
            sentences = request.annotations["sentence"]
            tokens_in = request.annotations["token"]
        tokens_out = self.filosoft_disamb(cmd, sentences, tokens_in)
        annotation_out = {'sentence': sentences, "token": tokens_out}
        anno_resp = AnnotationsResponse(annotations=annotation_out)
        return anno_resp

    def filosoft_disamb(self, cmd: List, sentences: List, tokens: List) -> List:
        # korjame tekstisõned kokku ja laseme morfist läbi
        conv = filosoft_morph.FS_IO_CONV()
        text = conv.elg_sentences_tokens_morf_2_fs_xml_morf(sentences, tokens)
        p = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
        lines = p.communicate(input=bytes(text, 'utf-8'))[0].decode('utf-8').split('\n')
        # tükeldame morf analüüsi tulemuse püütoni sõnastikuks ja
        # lisame analüüsid algsele andmestikule
        l = s = t = 0
        while s < len(sentences):
            assert(lines[l] == '<s>')
            l += 1
            while l < len(lines) and t < len(tokens) and tokens[t].end <= sentences[s].end:
                m = conv.fs_xml_morph_2_elg_morph(lines[l])
                assert(m["token"] == tokens[t].features["token"])
                tokens[t].features = m
                t += 1
                l += 1
            assert(lines[l] == '</s>')
            l += 1
            s += 1
        return tokens


flask_service = FiloSoft_disamb("Filosofti morfoloogiline ühestaja")
app = flask_service.app


def run_test(my_query_str: str) -> Dict:
    '''
    Run as command line script
    :param my_query_str: input in json string
    '''
    from elg.model import TextRequest
    my_query = json.loads(my_query_str)
    service = FiloSoft_disamb("Filosofti morfoloogiline ühestaja")
    if "params" not in my_query:
        my_query["params"] = {}
    if "annotations" in my_query:
        request = TextRequest(params=my_query["params"], content=my_query["content"], annotations=my_query["annotations"])
    else:
        request = TextRequest(params=my_query["params"], content=my_query["content"])
    response = service.process_text(request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True
    response_json_json = json.loads(response_json_str)
    return response_json_json


def run_server():
    '''
    Run as flask webserver
    '''
    app.run(port=8000)


if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='ELG compatible json')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        json.dump(run_test(args.json), sys.stdout, indent=4)
